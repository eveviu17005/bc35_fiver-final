import React from 'react'
import Banner from './Banner.jsx'
import Carousel from './Carousel.jsx'
import Infor from './Infor.jsx'
import SliderItem from './SliderItem.jsx'
import Testimonial from './Testimonial.jsx'
import TrustBy from './TrustBy'

export default function HomePage() {
  return (
    <div className='overflow-x-hidden  mx-auto '>
        <Banner/>
        <TrustBy/>
        <SliderItem/>
        <Infor/>
        <Carousel/>
        <Testimonial/>
    </div>
  )
}
