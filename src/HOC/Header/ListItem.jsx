import React from "react";
import { useState } from "react";
import { useEffect } from "react";
import { jobSevice } from "../../services/jobService";
import { NavLink } from "react-router-dom";
import { useDispatch } from "react-redux";
import { recieveId } from "../../redux-toolkit/headerItem";
import { recievedItem } from "../../redux-toolkit/itemTitleSlider";

export default function ListItem() {
  const [item, setItem] = useState([]);
  const dispatch = useDispatch();
  const [jobDetail, setJobDetail] = useState([]);
  const [filterItem, setFilterItem] = useState([]);
  
  useEffect(() => {
    jobSevice
      .getJobCategory()
      .then((res) => {
        console.log("res: ",res);
        setItem(res.data.content);
      })
      .catch((err) => {
        console.log("err: ",err);
        console.log(err);
      });
  }, []);

  const dispatchItem = (id) => {
    dispatch(recieveId(id));
    // window.location.href('/title')
  };

  const dispatchItemSlider = (item) => { 
    dispatch(recievedItem(item))
  
   }

  const filter = (idJob) => {
    const filteredItem = item?.filter((idJob123) => {
      return idJob123.id == idJob;
    });
    setFilterItem(filteredItem);
  };

  const mapItemDetail = (item) => {
    return item.map((i) => {
      return (
        <NavLink key={i.id} to={'/job'}>
          <p
            onClick={() => {
              dispatchItem(i.id);
            }}
            className="pb-2 item-p"
          >
            {i.job_name}
          </p>
        </NavLink>
      );
    });
  };

  const mapDetail = () => {
    return filterItem[0]?.detail_type_job?.map((i) => {
      return (
        <div key={i.id} className="button-hover container-fluid">
          <ul>
            <li>
              <h2 className="text-left pb-1 text-xl hoverable-detail font-bold">
                {i.detail_name}
              </h2>
            </li>
            <li>{mapItemDetail(i?.job)}</li>
          </ul>
        </div>
      );
    });
  };
  const mapItem = () => {
    return item?.map((item) => {
      return (
          <ul key={item.id} className="space-x-3 cursor-pointer job-item">
            <li id="job-item-li" className="job-item-li">
            <NavLink to={'/title'}>
            <h1
                onClick={() => { dispatchItemSlider(filterItem) }}
                onMouseOver={() => {
                  filter(item.id);
                }}
                className="job-category"
              >
                
                {item.name_job_type}
              </h1>
            </NavLink>
             
              <div id="job-detail" className="job-detail space-x-3 delay-150">
                {/* container item-detail mx-auto" */}
                <div className="container mx-auto text-left transition-all delay-500">
                  <NavLink className="w-full space-y-5">{mapDetail()}</NavLink>
                </div>
              </div>
            </li>
          </ul>
      );
    });
  };

  return (
    <div className="container mx-auto py-3">
      <div className="flex item-list justify-evenly">{mapItem()}</div>
    </div>
  );
}
