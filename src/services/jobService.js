import { https } from "./configUrl";
export const jobSevice = {
  getJobCategory: () => {
    return https.get("/job/menu");
  },
  getJobDetail: (id) => {
    return https.get(`/job/get-job-detail-type-job/${id}`);
  },
  getJobList: (id) => {
    return https.get(`/job/get-job/${id}`);
  },
  getJobAccordingToName: (name) => {
    return https.get(`/job/get-list-jobs-from-name/${name}`);
  },

  getDetailGig: (id) => {
    return https.get(`/job/get-detail-job/${id}`);
  },
  getGigBySellerId: (id) => {
    return https.get(`/api/thue-cong-viec/lay-danh-sach-da-thue/${id}`);
  },
  // getDetailSeller,
  getDetailSeller: (id) => {
    return https.get(`/api/users/${id}`);
  },
  getClientComment: (id) => {
    return https.get(`/api/binh-luan/lay-binh-luan-theo-cong-viec/${id}`);
  },
  createComment: (data) =>
    https.post(`/api/binh-luan`, {
      maCongViec: Number(data.maCongViec),
      maNguoiBinhLuan: data.maNguoiBinhLuan,
      noiDung: data.noiDung,
    }),
  getGigBySeller: () => {
    return https.get(
      "https://fiverrnew.cybersoft.edu.vn/api/thue-cong-viec/lay-danh-sach-da-thue"
    );
  },
};
