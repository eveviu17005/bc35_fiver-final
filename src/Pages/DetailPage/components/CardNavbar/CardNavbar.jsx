import React from "react";
import { RiArrowDropRightLine } from "react-icons/ri";
import "./CardNavbar.scss";
export default function CardNavbar({ navbar, detail }) {
  return (
    <nav className=" categories-breadcrumbs">
      <ul className="flex items-center">
        <li className="flex items-center">
          <span className="category-breadcrumbs ">
            <a href="#">{detail?.detail_type_job.type_job.name_job_type}</a>
          </span>
          <RiArrowDropRightLine size={30} />
          <span className="category-breadcrumbs ">
            <a href="#">{detail?.detail_type_job.detail_name}</a>
          </span>
          <RiArrowDropRightLine size={30} />
          <span className="category-breadcrumbs ">
            <a href="#">{detail?.job_name}</a>
          </span>
        </li>
      </ul>
    </nav>
  );
}
