import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';
import liveReload from 'vite-plugin-live-reload';
import dns from 'dns';
dns.setDefaultResultOrder('verbatim');
export default defineConfig(() => {
  return {
    build: {
      outDir: 'build',
    },
    server: {
        open: true,
        host: 'localhost',
        port: 3000,
        hmr: false,
      },
    plugins: [react(),liveReload('')],
  };
  
});